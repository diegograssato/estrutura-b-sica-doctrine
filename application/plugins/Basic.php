<?php

use Improver\Utils\LogManager;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Application_Plugin_Basic
 *
 * @author maatao
 */
class Application_Plugin_Basic extends Zend_Controller_Plugin_Abstract{

    private $_view;
    
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request) {
        parent::dispatchLoopStartup($request);
        
        $layout = Zend_Layout::getMvcInstance();
        $this->_view = $layout->getView();
        
        $jquery  = '/js/jquery-1.7.2.js';
        
        $this->_view->headScript()->appendFile($jquery);
    }
}

