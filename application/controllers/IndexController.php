<?php

use Improver\Utils\LogManager;
use GuestBook\Model\EntityMapper\GuestBookMapper;
use GuestBook\Model\BusinessLayer\GuestBook;
use Authenticator\Model\BusinessLayer\User;
use GuestBook\Model\BusinessLayer\Signature;

class IndexController extends Zend_Controller_Action {

    private $user;

    public function init() {
        /* Initialize action controller here */
        try {
            $user = User::getUser(1);
        } catch (\Exception $e) {
            LogManager::getInstance()->logExceptionMessage('Erro ao obter o user default');
            LogManager::getInstance()->logExceptionMessage($e->getMessage());
            // Apenas repassando
            throw $e;
        }
        $this->user = $user;
    }

    public function indexAction() {
        
    }

    /*     * Adiciona uma assinatura no sistema. */

    public function addsignatureAction() {

        // Desabilitando o layout desta Action
        $this->_helper->layout->disableLayout();

        /*
         * Pseudo: 
         * 1) obtem o usuario (user_id)
         * 2) obtem o livro de visitar (guestbook_id)
         * 3) adiciona a visita
         */

        $request = $this->getRequest();

        if ($request->isPost()) {
            // Obtendo os parametros
            $user_id = $request->getParam('user_id');

            if ($user_id == null) {
                throw new \Zend_Exception('Parâmetro user_id não informado. ');
            }

            try{
                $user = User::getUser($user_id);
            }catch(\Exception $e){
                LogManager::getInstance()->logExceptionMessage('Erro ao obter o usuário.');
                LogManager::getInstance()->logExceptionMessage($e->getMessage());
                // Apenas repassando 
                throw $e;
            }
            
            if (!($user instanceof User)) {
                throw new \Zend_Exception('Usuário não existe. (user_id = ' . $user_id . ')');
            }

            $guestbook_id = $request->getParam('guestbook_id');

            if ($guestbook_id == null) {
                throw new \Zend_Exception('Parâmetro guestbook_id não informado. ');
            }

            $guestbook = GuestBook::getGuestBook($guestbook_id);

            if (!($guestbook instanceof GuestBook)) {
                throw new \Zend_Exception('Livro de visitas não existe. (guestbook_id = ' . $guestbook_id . ')');
            }

            $mensagem = $request->getParam('mensagem');

            $data_criacao = new \DateTime('now');

            // Criando a assinatura
            $signature = new Signature();
            $signature->setCreationDate($data_criacao);
            $signature->setGuestBook($guestbook);
            $signature->setMessage($mensagem);
            $signature->setUser($user);
            
            // Gravando 
            try {
                $signature->saveSignature();
            } catch (\Exception $e) {
                LogManager::getInstance()->logExceptionMessage('Erro ao gravar a assinatura. ');
                LogManager::getInstance()->logExceptionMessage($e->getMessage());
                // Apenas repassando
                throw $e;
            }

            $this->view->message = 'OK';
        } else {
            // Nada, Esperando um post
        }
    }

    public function guestbookAction() {

    }

    public function listsignaturesAction() {

        // Desabilitando o layout
        $this->_helper->layout->disableLayout();



        /*
         * Pseudo: 
         * obtem o id do livro de visitas (guest_book_id)
         * obtem a pagina atual a ser visualizada (pag) 
         * lista as assinaturas para aquele livro 
         * 
         * 
         */

        $request = $this->getRequest();

        if ($request->isPost()) {

            $guest_book_id = $request->getParam('guest_book_id');
            
            if($guest_book_id == null){
                LogManager::getInstance()->logErrorMessage('Parâmetro guest_book_id não informado. ');
            }

            try {
                $guest_book = GuestBook::getGuestBook($guest_book_id);
            } catch (\Exception $e) {
                LogManager::getInstance()->logExceptionMessage('Erro ao obter o guestbook.');
                LogManager::getInstance()->logExceptionMessage($e->getMessage());
                // Apenas repassando
                throw $e;
            }

            if (!$guest_book instanceof GuestBook) {
                
            }

            // Obtendo a lista de assinaturas para o guestbook atual.
            try {
                $signature_paginator = $guest_book->listSignatures();
            } catch (\Exception $e) {
                LogManager::getInstance()->logExceptionMessage('Erro ao obter a lista de assinaturas. ');
                LogManager::getInstance()->logExceptionMessage($e->getMessage());
                // Apenas repassando 
                throw $e;
            }

            // Convertendo o paginator para uma lista de objetos
            $paginator = new Zend_Paginator($signature_paginator);
            $page_number = $request->getParam('pag');
            if ($page_number == null) {
                $page_number = 1;
            }

            $paginator->setCurrentPageNumber($page_number);
            $itemCountPerPage = Zend_Registry::get('config')->resource->pagination->itemcountperpage;
            $paginator->setItemCountPerPage($itemCountPerPage);


            $signature_list = array();

            foreach ($paginator as $object) {
                $signature_list[] = $object;
            }

            $this->view->signature_list = $signature_list;
            $this->view->guest_book = $guest_book;
            $this->view->paginator = $paginator;
        } else {
            
        }
    }
}

