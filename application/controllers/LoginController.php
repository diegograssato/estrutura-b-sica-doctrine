<?php

use Improver\Utils\LogManager;
use Improver\Utils\DoctrineAdapter;
use Authenticator\Interfaces\InterfaceLoginController;

class LoginController extends Zend_Controller_Action implements InterfaceLoginController {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $params = array('module' => 'default', 'controller' => 'login', 'action' => 'login');
        $url = $this->view->url($params);

        $this->_redirect($url);
    }

    public function loginAction() {

        $login = new Application_Form_Auth_SimpleLogin();

        $request = $this->getRequest();
        
        if($request->isPost()){  
            
            if ($login->isValid($_POST)) {
                $params = array('module' => 'default', 'controller' => 'index', 'action' => 'index');
                $url = $this->view->url($params);

                /*
                * Pseudo:
                * 1) Recupera login e password do formulario
                * 2) Encripta de acordo (decidir algoritmo) - class Authenticator
                * 3) Chama método do usuário para validar (Negocio -> DAO) - class Autheticator
                * 4) Se OK o DAO retorna o usuario - caso contrário null
                * 5) Retorna true ou false (indicando se a autenticacao foi sucedida)
                */

                $identifier = $login->getValue('username');
                $password = $login->getValue('password');

                $adapter = new DoctrineAdapter($identifier, $password);

                $result = Zend_Auth::getInstance()->authenticate($adapter);

                if ($result) {
                    $session_namespace = Zend_Registry::get('session_namespace');
                    $session_namespace->expiration = true;
                    
                    $this->_redirect($url);
                    return;
                }
            } else {

            }
        }
        
        $this->view->form_login = $login;
    }

    public function logoutAction() {
        $params = array('module' => 'default', 'controller' => 'index', 'action' => 'index');
        $url = $this->view->url($params);
        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::destroy();

        $this->_redirect($url);
    }
}

