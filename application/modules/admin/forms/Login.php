<?php

class Admin_Form_Login extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        
        $this->setName('login');
        $this->setAttrib('id', 'login');
        $this->setMethod('post');
        $this->setAttrib('class', 'zend_form');
        
        
        $form = new Zend_Form();
        
        $user = $form->createElement('text', 'user', 
                array(
                 'label' => 'Titulo',
                 'maxlength' => '20',
                 'required' => true,   
                ))->addFilter('StringTrim');
        
        $password = $form->createElement('password', 'password',
                array(
                    'label' => 'Senha',
                    'malength' => '20',
                    'required' => true,
                    'filters'  => array('StringTrim'),
                )
                );
        
        $this->addElement($user);
        $this->addElement($password);
    }
}

