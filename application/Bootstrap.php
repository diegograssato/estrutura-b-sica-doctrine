<?php

use Improver\Utils\ACLImprover;

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    // Adicionando um recurso do bootstrap
    protected function _initDoctype() {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');
    }

    /**
     * 
     * Inicializando o arquivo de configuração e habilitando ele no Zend_Registry. 
     * Os recursos existentes no arquivo .ini vão estar disponíveis para utilização.  
     * 
     */
    public function _initConfiguration() {
        $config = new Zend_Config($this->getOptions());
        Zend_Registry::set('config', $config);
    }

    public function _initAutoloaderNamespaces() {
        require_once('Doctrine/Common/ClassLoader.php');

        $autoloader = Zend_Loader_Autoloader::getInstance();

        $class_loader = new \Doctrine\Common\ClassLoader('Bisna');
        $autoloader->pushAutoloader(array($class_loader, 'loadClass'), 'Bisna');


        $entity = Zend_Registry::get('config')->resource->entityManager->connection->entities;

        $class_loader = new \Doctrine\Common\ClassLoader('Entities',
                        $entity,
                        'loadClass');
        $autoloader->pushAutoloader(array($class_loader, 'loadClass'), 'Entities');
    }

    protected function _initPlugin() {

        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Application_Plugin_Basic());
        //$front->dispatch();
    }

    /*     * Registra o plugin do ACL */

    protected function _initACL() {
        $front = Zend_Controller_Front::getInstance();

        $acl = new ACLImprover();

        $front->registerPlugin(new Application_Plugin_ACL($acl));
    }
    
    protected function _initSession(){        
        $session_namespace = new Zend_Session_Namespace('login');
        // Expira em 10 minutos
        $session_namespace->setExpirationSeconds(600);

        Zend_Registry::set('session_namespace', $session_namespace);
    }

    protected function _initRouter(){
        
        return;
        
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();
        
        $route = new Zend_Controller_Router_Route(
                'default/login',
                array(
                    'module' => 'default',
                    'controller' => 'login',
                    'action' => 'login'
                    )
                );
        $router->addRoute('login_default',$route);
        
        
    }
    
    
    protected function _initTranslate() {
        return;
        // TODO: ver isto depois
        $translator = new Zend_Translate(
                        array(
                            'adapter' => 'array',
                            'content' => '/resources/languages',
                            'locale' => Zend_Translate::$language,
                            'scan' => Zend_Translate::LOCALE_DIRECTORY
                        )
        );
        Zend_Validate_Abstract::setDefaultTranslator($translator);
    }

}

