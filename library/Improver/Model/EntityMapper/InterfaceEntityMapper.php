<?php

namespace Improver\Model\EntityMapper;


/**
 *
 * @author maatao
 */
interface InterfaceEntityMapper {
    
    /**
     * Retorna a instância do Mapper. Implementado via Singleton. 
     */
    public static function getInstance();
    
    /**
     * Mapeia uma entidade de negócio em uma entidade Doctrine.
     * 
     */
    public function businnessEntityToDoctrine($object);
    /**
     * 
     * Mapeia uma entidade Doctrine em uma entidade de negócio. 
     * 
     */
    public function doctrineEntityToBusiness($object);
}

