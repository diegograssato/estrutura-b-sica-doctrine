<?php

namespace Improver\Model\Doctrine\DAO;

/**
 *
 * @author maatao
 */
interface InterfaceObjectDAO {
    
    /** 
     * 
     * Adiciona um objeto (representado por uma Entity do BusinessLayer)
     * no Banco de Dados.  
     * 
     */
    public static function add($object);
    /**
     * Remove um objeto (representado por uma Entity do BusinessLayer) do 
     * Banco de Dados. 
     * <br />
     * Se acaso o objeto não existe gera uma Zend_Exception. 
     * 
     */
    public static function delete($object);
    /**
     * 
     * Retorna um elemento específico (identificado pelo id passado).
     * <br />
     * Retorna null se acaso o elemento não existir.
     * @param id o identificador do elemento. 
     * 
     */
    public static function get($id);
    
    /**
     * 
     * Lista todos os elementos existentes desta entidade. Não impõe restrição 
     * nesta listagem. 
     * <br />
     * 
     * @param $paginated_list indica se o resultado vai vir paginado (objeto da 
     * classe PaginationAdapter) ou não (um array de objetos). Retorna um array 
     * vazio se acaso $paginated_list = false e não existir elementos a listar. 
     * 
     */
    public static function listAll($paginated_list = true);
}

