<?php

namespace Improver\Model\Doctrine\Entity;

/**
 * 
 * @Table(name="log")
 * @Entity
 */
class LogManagerDoctrine {
    /**
     * @var integer $id
     * @Column(type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    public function setId($id){$this->id = $id;}
    public function getId(){return $this->id;}
    
    
    /**
     * @Column(type="string")
     */
    private $user;
    
    public function setUser($object){$this->user=$object;}
    public function getUser(){return $this->user;}

    /**
     * @Column(type="string")
     */
    private $priority_name;

    public function setPriorityName($object){$this->priority_name=$object;}
    public function getPriorityName(){return $this->priority_name;}
    
    /**
     * @Column(type="datetime")
     */
    
    private $creation_date;
    
    /**
     * @Column(type="integer")
     */
    
    private $priority;
    
    /**
     * @Column(type="string")
     */
    private $message;
    
    
    
}

