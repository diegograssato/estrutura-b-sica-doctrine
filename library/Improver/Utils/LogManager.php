<?php

namespace Improver\Utils;

use Zend\Zend_Registry;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LogManager
 *
 * @author maatao
 */
class LogManager {
    
    /**Loga as informações no Banco de Dados*/
    private $logDB;
    
    public function setLogDB($logDB){$this->logDB=$logDB;}
    public function getLogDB(){return $this->logDB;}
    
    
    /**Os parâmetros utilizados no Log. */
    private $params;
    
    private static $instance = null;
    
    private function __construct(){
        $this->params = array('user' => 'user', 'priority_name' => 'priorityName', 
            'creation_date' => 'timestamp', 'priority' => 'priority', 'message' => 'message');
    }
    
    private function __clone(){}
    
    public static function getInstance(){
        if(self::$instance === null){
            
            // Gerando a entidade LogManager
            self::$instance = new self();               
            
            $doctrine_container = \Zend_Registry::get('doctrine');
            $db = $doctrine_container->getConnection();

            $writer = new \Zend_Log_Writer_Db($db, 'log', self::$instance->params);
            $logDB = new \Zend_Log($writer);


            // REgistrando o $logDB no $logManager
            self::$instance->setLogDB($logDB);                                  
        }
        return self::$instance;
        
    }
    
    
    /**
     * 
     * Loga uma informação com nível de prioridade INFO. 
     * 
     * @param $user o id do usário que estava interagindo com o sistema (caso não seja 
     * passado é considerado o id = -1)
     * 
     */
    public function logInfoMessage($message, $user = -1){
        $this->logDB->log($message, \Zend_Log::INFO,array('user'=> $user));
    }
    /**
     * 
     * Loga uma informação com nível de prioridade de Exception (EMERG). 
     * 
     * @param $user o id do usário que estava interagindo com o sistema (caso não seja 
     * passado é considerado o id = -1)
     * 
     */
    public function logExceptionMessage($message, $user = -1){
        $this->logDB->log($message, \Zend_Log::EMERG, array('user' => $user));
    }
    
    /**
     * 
     * Loga uma informação com nível de prioridade de Error (ERR). 
     * 
     * @param $user o id do usário que estava interagindo com o sistema (caso não seja 
     * passado é considerado o id = -1)
     * 
     */
    public function logErrorMessage($message, $user = -1){
        $this->logDB->log($message, \Zend_Log::ERR, array('user' => $user));
    }
}
