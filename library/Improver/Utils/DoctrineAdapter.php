<?php

namespace Improver\Utils;

use Zend\Zend_Auth_Adapter_Interface;
use Zend\Zend_Auth_Result;
use Authenticator\Model\BusinessLayer\User;
use Improver\Utils\LogManager;


/**
 * Adapter utilizado para autenticar o usuário no sistema.
 *
 * @author maatao
 */
class DoctrineAdapter implements \Zend_Auth_Adapter_Interface {

    private $username;
    private $password;
    
    public function __construct($username, $password){
        $this->username = $username;
        $this->password = $password;
    }
    
    /**
     * 
     * Realiza uma tentativa de autenticação do usário.
     * 
     * @throws Zend_Auth_Adapter_Exception se a autenticação não puder ser realizada
     * @return Zend_Auth_Result
     */
    public function authenticate() {
        /*
         * 
         * Pseudo:
         * 
         * 1) Tenta realizar a autenticação do usuário no sistema. 
         * 2) Se resultado positivo (retorna o result indicando isto)
         * 3) Se negativo (retorna o result indicando isto)
         * 
         */
        
        try{
            
            // Recuperando o campo salt do usuario
            $salt = User::getSaltByIdentifier($this->username);
            
            $password_with_salt = $this->password . $salt;
            
            $password_with_crypt =  hash('whirlpool',$password_with_salt);
            
            $user = User::validateSystemUser($this->username, $password_with_crypt); 
            
            $identity = array();
            $identity['id'] = $user->getId();
            $identity['login'] = $user->getLogin();
            $identity['role_id'] = $user->getRole()->getId();
            $identity['role_name'] = $user->getRole()->getName();
            $code = \Zend_Auth_Result::SUCCESS;
            $messages = array();
            
        }catch(\Exception $e){
            $messages = array();
            $identity = null;
            if($e->getMessage() == \Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND){
                $code = \Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
                $messages[] = 'Usuário não encontrado';                
            }else if($e->getMessage() == \Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS){
                $code = \Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS;
                $messages[] = 'Ambiguidade na identidade';
            }else if($e->getMessage() == \Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID){
                $code = \Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
                $messages[] = 'Password inválido';
            }
        }
        
        $result = new \Zend_Auth_Result($code, $identity, $messages);

        return $result;
    }
}

