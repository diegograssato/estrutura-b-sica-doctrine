<?php

namespace Authenticator\Model\EntityMapper;

use Authenticator\Model\BusinessLayer\User;
use Authenticator\Model\Doctrine\Entity\UserDoctrine;
use Authenticator\Utils\LogManager;
use Improver\Model\EntityMapper\InterfaceEntityMapper;  


class UserMapper implements InterfaceEntityMapper{
    
    private static $instance = null;
    
    private function __construct(){}
    private function __clone(){}
    
    public static function getInstance(){
        if(UserMapper::$instance == null){
            UserMapper::$instance = new UserMapper();
        }
        return UserMapper::$instance;
    }
    
    public function businnessEntityToDoctrine($object) {
        if(!($object instanceof User)){
         throw new \Zend_Exception('Objeto passado não é uma instancia de Authenticator\Model\BusinessLayer\User');
        }
        
       // Converte entre os objetos e retorna uma instancia do objeto do doctrine
       $entity_doctrine = new UserDoctrine();
       $entity_doctrine->setId($object->getId());  
       $entity_doctrine->setLogin($object->getLogin());
       $entity_doctrine->setCreationDate($object->getCreationDate());
       $entity_doctrine->setRole($object->getRole());
       $entity_doctrine->setSalt($object->getSalt());
       
       return $entity_doctrine;
    }

    public function doctrineEntityToBusiness($object) {
        if(!($object instanceof UserDoctrine)){
           throw new \Zend_Exception('Objeto passado não é uma instância de Authenticator\Model\Doctrine\Entity\UserDoctrine');
        }
        
        // Converte entre os objetos e retorna uma instancia do objeto de negócio 
        $entity_business = new User();
        $entity_business->setId($object->getId());
        $entity_business->setLogin($object->getLogin());
        $entity_business->setCreationDate($object->getCreationDate());
        $entity_business->setRole($object->getRole());
        $entity_business->setSalt($object->getSalt());
        
        return $entity_business;
    }
    
}

