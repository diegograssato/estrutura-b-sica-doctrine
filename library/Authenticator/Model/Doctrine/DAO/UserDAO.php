<?php

namespace Authenticator\Model\Doctrine\DAO;

use Improver\Model\Doctrine\DAO\InterfaceObjectDAO;
use Zend\Zend_Registry;
use Authenticator\Model\BusinessLayer\User;
use Authenticator\Model\Doctrine\Entity\UserDoctrine;
use Authenticator\Model\EntityMapper\UserMapper;

/**
 * Description of User
 *
 * @author maatao
 */
class UserDAO implements InterfaceObjectDAO{
    
    private static $business_class_namespace = 'Authenticator\Model\BusinessLayer';
    private static $doctrine_class_namespace = 'Authenticator\Model\Doctrine\Entity';

    
    public static function add($object) {
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();

        if(!($object instanceof User)){
            throw new \Zend_Exception('Objeto não é uma instância de ' . self::$business_class_namespace . '\User');
        }
        
        $object_doctrine = UserMapper::getInstance()->businnessEntityToDoctrine($object);
        
        if($object_doctrine->getId()){
            $em->merge($object_doctrine);
        }else{
            $em->persist($object_doctrine);
        }
        
        $em->flush();
        
        // Retornando o objeto inserido com o valor da chave atribuido
        return UserMapper::getInstance()->doctrineEntityToBusiness($object_doctrine);
    }
    
    public static function delete($object) {

        if(!($object instanceof User)){
            throw new \Zend_Exception('Objeto não é uma instância de ' . self::$business_class_namespace . '\User');
        }
        
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();
        
        $element = $em->getRepository(self::$doctrine_class_namespace . '\UserDoctrine')->find($object->getId());;
        
        if(!($element instanceof UserDoctrine)){
            // Nao existe o objeto no Banco de Dados
            throw new \Zend_Exception('O objeto indicado não existe no Banco de Dados. ');
        }
        
        $em->remove($element);
        $em->flush();        
        
        // Retornando o objeto removido
        return UserMapper::getInstance()->doctrineEntityToBusiness($element);
    }
    public static function get($id) {
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();
                       
        $user = $em->getRepository(self::$doctrine_class_namespace . '\UserDoctrine')->find($id);
         
        if(!($user instanceof UserDoctrine)){
            return null;
        }
        
        return UserMapper::getInstance()->doctrineEntityToBusiness($user);        
    } // function
    
    public static function listAll($paginated_list = true){
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();
                       
        $query = $em->createQuery('select s from ' . self::$doctrine_class_namespace . '\UserDoctrine s');
        
        if($paginated_list){
            // Paginação habilitada
        }
                
        $object_list = $query->getResult();
        
        $object_business_list = array();
        foreach($object_list as $object){
            $object_business_list[] = UserMapper::getInstance()->doctrineEntityToBusiness($object);
        }
        
        return $object_business_list;        
    }
    
    
    /**
     * 
     * Valida o usuário no sistema, i. e., verifica se existe o usuário no sistema
     * e se o password deste está correto. 
     * 
     */   
    public static function validateSystemUser($identifier, $password){
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();
                       
        $query = $em->createQuery('select u from ' . self::$doctrine_class_namespace . 
                '\UserDoctrine u WHERE u.login = ?1');
        
        if(!$query instanceof \Doctrine\ORM\Query ){
            
        }
        
        $query->setParameter(1,$identifier);
        
        $object_doctrine = $query->getResult();
        
        if(count($object_doctrine) == 0){
            $message = \Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
            throw new \Zend_Exception($message);                
        }else if(count($object_doctrine) != 1){
            $message = \Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS;
            throw new \Zend_Exception($message);                
        }
        
        $object_doctrine = $object_doctrine[0];

        if(!$object_doctrine instanceof UserDoctrine){
            throw new \Zend_Excpetion('Objeto user não é uma instância de UserDoctrine');
        }
        
        if($object_doctrine->getPassword() !== $password){
            $message = \Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
            throw new \Exception($message);
        }
        
                
        try{
            $object_business = UserMapper::getInstance()->doctrineEntityToBusiness($object_doctrine);        
        }catch(Exception $e){
            LogManager::getInstance()->logErrorMessage('Tentativa de login inválida. (user: ' . $identifier . 
                    ', password: ' . $password);
            // Tratando a exception 
            $object_business = null; 
        }
        
        return $object_business;
    }

    public static function getSaltByIdentifier($identifier) {
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();
                       
        $query = $em->createQuery('select u from ' . self::$doctrine_class_namespace . 
                '\UserDoctrine u WHERE u.login = ?1');
        
        if(!$query instanceof \Doctrine\ORM\Query ){
            
        }
        
        $query->setParameter(1,$identifier);
        
        $object_doctrine = $query->getResult();
        
        if(count($object_doctrine) === 0){
            throw new \Zend_Exception('Erro ao recuperar o salt para o usuário. Usuário não foi encontrado no BD.  ');
        }

        if(count($object_doctrine) !== 1){
            throw new Zend_Exception('Erro ao recuperar o salt para o usuário. Conteúdo duplicado. ');
        }
        
        $object_doctrine = $object_doctrine[0];
        
        if(!($object_doctrine instanceof UserDoctrine)){
            throw new \Zend_Excpetion('Objeto user não é uma instância de UserDoctrine');
        }
        
        $salt = $object_doctrine->getSalt();
        
        return $salt;        
    } // function
    
    
} // class

