<?php

namespace Authenticator\Model\Doctrine\Entity;

/**
 * 
 * @Table(name="user")
 * @Entity
 */
class UserDoctrine{
    
    /**
     * @var integer $id
     * @Column(type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    public function setId($id){$this->id = $id;}
    public function getId(){return $this->id;}
    
    
    /**
     * @Column(type="string", length="100")
     */
    private $login;
    public function setLogin($login){$this->login=$login;}
    public function getLogin(){return $this->login;}
    
    /**
     * 
     * Representa a senha do usuário. O valor deste atributo é dada pela senha
     * original (que apenas o usuário conhece) acrescida do salt, sendo que 
     * o resultado disto é passado por um algoritmo de cipher. 
     * 
     * @Column(type="string", length="256")
     */
    private $password;
    public function setPassword($password){$this->password=$password;}
    public function getPassword(){return $this->password;}
    
    
    /**
     * Salt para redução da facilidade de ataque brute force. 
     * 
     * @Column(type="string", length="100")
     * 
     */
    private $salt;
    public function setSalt($salt){$this->salt=$salt;}
    public function getSalt(){return $this->salt;}
   
    /**
     * 
     * O papel (role) do usuário no sistema. 
     * 
     * @ManyToOne(targetEntity="Authenticator\Model\Doctrine\Entity\RoleDoctrine", inversedBy="users")
     * @JoinColumn(name="role_id",referencedColumnName="id", onDelete="CASCADE")
     * 
     */
    private $role;
    public function setRole($role){$this->role=$role;}
    public function getRole(){return $this->role;}
    
    /**
     * @Column(type="datetime")
     */
    private $creation_date;
    
    public function setCreationDate($creation_date){$this->creation_date=$creation_date;}
    public function getCreationDate(){return $this->creation_date;}
}
