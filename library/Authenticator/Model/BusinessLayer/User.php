<?php

namespace Authenticator\Model\BusinessLayer;

use Authenticator\Model\Doctrine\DAO\UserDAO;

/**
 * Representa um usuário do sistema. 
 *
 * @author maatao
 */
class User {
    
    private $id;
    public function setId($id){$this->id = $id;}
    public function getId(){return $this->id;}
    
    private $login;
    public function setLogin($login){$this->login=$login;}
    public function getLogin(){return $this->login;}

    
    private $role;
    public function setRole($role){$this->role=$role;}
    public function getRole(){return $this->role;}

    private $salt;
    public function setSalt($salt){$this->salt=$salt;}
    public function getSalt(){return $this->salt;}
    
    private $creation_date;
    public function setCreationDate($creation_date){$this->creation_date=$creation_date;}
    public function getCreationDate(){return $this->creation_date;}
    
    /**
     * 
     * Indica se o usuário se os dados fornecidos conseguem validar o 
     * usuário como autêntico. 
     * 
     * @param $identifier o parametro que identifica o usuario no sistema
     * @param $password a senha do usuário (já sobre efeito do algorimto de 
     * encriptação)
     * 
     */
    public static function validateSystemUser($identifier, $password){
        return UserDAO::validateSystemUser($identifier, $password);
    }
    

    public static function getSaltByIdentifier($identifier){
        return UserDAO::getSaltByIdentifier($identifier);
    }


    /**Obtem um determinado objeto (identificado pelo id passado) 
     * user do Banco de Dados*/
    public static function getUser($id){
        return UserDAO::get($id);
    }
    
    /**Retorna a lista de Usuarios existente no BD*/
    public static function listUsers(){
        return UserDAO::listAll();
    }

    /**Adiciona o usuario no Banco de Dados*/
    public function saveUser(){
        return UserDAO::add($this);
    }
}

