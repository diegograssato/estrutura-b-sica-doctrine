<?php

namespace Authenticator\Interfaces;


/**
 * Interface para implementação dos controller para autenticação do usuário. 
 * 
 * 
 * @author maatao
 */
interface InterfaceLoginController {
    
    /**Método para a autenticação do usuário no sistema. */
    public function loginAction();
    /**Método para a desautenticação do usuário. */
    public function logoutAction();
}


