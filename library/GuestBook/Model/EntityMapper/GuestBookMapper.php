<?php

namespace GuestBook\Model\EntityMapper;

use Improver\Model\EntityMapper\InterfaceEntityMapper;
use GuestBook\Model\BusinessLayer\GuestBook;
use GuestBook\Model\Doctrine\Entity\GuestBookDoctrine;
use Improver\Utils\LogManager;


class GuestBookMapper implements InterfaceEntityMapper{
    
    private static $instance = null;
    
    private function __construct(){}
    private function __clone(){}
    
    public static function getInstance(){
      if(GuestBookMapper::$instance == null){
          GuestBookMapper::$instance = new GuestBookMapper();
      }  
      return GuestBookMapper::$instance;
    }
    
    public function businnessEntityToDoctrine($object) {
        if(!($object instanceof GuestBook)){
         throw new \Zend_Exception('Objeto passado não é uma instancia de GuestBook\Model\BusinessLayer\GuestBook');
        }
        
       // Converte entre os objetos e retorna uma instancia do objeto do doctrine
       $entity_doctrine = new GuestBookDoctrine();
       $entity_doctrine->setId($object->getId());  
       $entity_doctrine->setTitle($object->getTitle());
       $entity_doctrine->setCreationDate($object->getCreateionDate());
       
       return $entity_doctrine;
    }

    public function doctrineEntityToBusiness($object) {
        if(!($object instanceof GuestBookDoctrine)){
           throw new \Zend_Exception('Objeto passado não é uma instância de GuestBook\Model\Doctrine\Entity\GuestBookDoctrine');
        }
        
        // Converte entre os objetos e retorna uma instancia do objeto de negócio 
        $entity_business = new GuestBook();
        $entity_business->setId($object->getId());
        $entity_business->setTitle($object->getTitle());
        $entity_business->setCreationDate($object->getCreationDate());
        
        return $entity_business;
    }
    
}

