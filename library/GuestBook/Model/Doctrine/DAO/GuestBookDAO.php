<?php

namespace GuestBook\Model\Doctrine\DAO;

use Improver\Model\Doctrine\DAO\InterfaceObjectDAO;
use Zend\Zend_Registry;
use GuestBook\Model\BusinessLayer\GuestBook;
use GuestBook\Model\Doctrine\Entity\GuestBookDoctrine;
use GuestBook\Model\EntityMapper\GuestBookMapper;

/**
 * Description of GuestBook
 *
 * @author maatao
 */
class GuestBookDAO implements InterfaceObjectDAO{
   
    private static $business_class_namespace = 'GuestBook\Model\BusinessLayer';
    private static $doctrine_class_namespace = 'GuestBook\Model\Doctrine\Entity';

    
    public static function add($object) {
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();

        if(!($object instanceof GuestBook)){
            throw new \Zend_Exception('Objeto não é uma instância de ' . self::$business_class_namespace . '\GuestBook');
        }
        
        $object_doctrine = GuestBookMapper::getInstance()->businnessEntityToDoctrine($object);
        
        if($object_doctrine->getId()){
            $em->merge($object_doctrine);
        }else{
            $em->persist($object_doctrine);
        }
        
        $em->flush();
        
        // Retornando o objeto inserido com o valor da chave atribuido
        return GuestBookMapper::getInstance()->doctrineEntityToBusiness($object_doctrine);
    }
    
    public static function delete($object) {

        if(!($object instanceof GuestBook)){
            throw new \Zend_Exception('Objeto não é uma instância de ' . self::$business_class_namespace . '\GuestBook');
        }
        
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();
        
        $element = $em->getRepository(self::$doctrine_class_namespace . '\GuestBookDoctrine')->find($object->getId());;
        
        if(!($element instanceof GuestBookDoctrine)){
            // Nao existe o objeto no Banco de Dados
            throw new \Zend_Exception('O objeto indicado não existe no Banco de Dados. ');
        }
        
        $em->remove($element);
        $em->flush();        
       
        // Retornando o objeto removido
        return GuestBookMapper::getInstance()->doctrineEntityToBusiness($element);
    }
    public static function get($id) {
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();
                       
        $guestbook = $em->getRepository(self::$doctrine_class_namespace . '\GuestBookDoctrine')->find($id);
         
        if(!($guestbook instanceof GuestBookDoctrine)){
            return null;
        }
        
        return GuestBookMapper::getInstance()->doctrineEntityToBusiness($guestbook);        
    } // function
    
    public static function listAll($paginated_list = true){
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();
        
        $query = $em->createQuery('select obj from ' . self::$doctrine_class_namespace . '\GuestBook obj');
        
        if($paginated_list){
            // Paginação ativada
            $mapper = SignatureMapper::getInstance();
            
            return new \DoctrineExtensions\Paginate\PaginationAdapter($query, $mapper, null);
        }
                
        $object_list = $query->getResult();
        
        $object_business_list = array();
        foreach($object_list as $object){
            $object_business_list[] = GuestBookMapper::getInstance()->doctrineEntityToBusiness($object);
        }
        
        return $object_business_list;        
    }
    
} // class

