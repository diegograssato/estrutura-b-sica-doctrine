<?php

namespace GuestBook\Model\Doctrine\DAO;

use Improver\Model\Doctrine\DAO\InterfaceObjectDAO;
use Zend\Zend_Registry;
use GuestBook\Model\BusinessLayer\Signature;
use GuestBook\Model\Doctrine\Entity\SignatureDoctrine;
use GuestBook\Model\EntityMapper\SignatureMapper;

/**
 * Description of GuestBook
 *
 * @author maatao
 */
class SignatureDAO implements InterfaceObjectDAO{

    private static $business_class_namespace = 'GuestBook\Model\BusinessLayer';
    private static $doctrine_class_namespace = 'GuestBook\Model\Doctrine\Entity';

    public static function add($object) {
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();

        if(!($object instanceof Signature)){
            throw new \Zend_Exception('Objeto não é uma instância de ' . self::$business_class_namespace . '\Signature');
        }
        
        $object_doctrine = SignatureMapper::getInstance()->businnessEntityToDoctrine($object);
        
        if(!($object_doctrine instanceof SignatureDoctrine)){
            throw new \Zend_Exception('Objeto não é uma instância de SignatureDoctrine');
        }
        
        //merge dos elementos
      
        // User
        $user = $em->merge($object_doctrine->getUser());
        $object_doctrine->setUser($user);
        
        // Guestbook
        $guestbook = $em->merge($object_doctrine->getGuestBook());
        $object_doctrine->setGuestBook($guestbook);
        
        if($object_doctrine->getId()){
            $em->merge($object_doctrine);
        }else{
            $em->persist($object_doctrine);
        }
        
        $em->flush();
        
        // Retornando o objeto inserido com o valor da chave atribuido
        return SignatureMapper::getInstance()->doctrineEntityToBusiness($object_doctrine);
    }
    
    public static function delete($object) {

        if(!($object instanceof Signature)){
            throw new \Zend_Exception('Objeto não é uma instância de ' . self::$business_class_namespace . '\Signature');
        }
        
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();
        
        $element = $em->getRepository(self::$doctrine_class_namespace.'\SignatureDoctrine')->find($object->getId());;
        
        if(!($element instanceof SignatureDoctrine)){
            // Nao existe o objeto no Banco de Dados
            throw new \Zend_Exception('O objeto indicado não existe no Banco de Dados. ');
        }
        
        $em->remove($element);
        $em->flush();        
        
        // Retornando o objeto removido
        return SignatureMapper::getInstance()->doctrineEntityToBusiness($element);
    }
    public static function get($id) {
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();
                       
        $object = $em->getRepository(self::$doctrine_class_namespace.'\SignatureDoctrine')->find($id);
         
        if(!($object instanceof SignatureDoctrine)){
            return null;
        }
        
        return SignatureMapper::getInstance()->doctrineEntityToBusiness($object);        
    } // function
    
    public static function listAll($paginated_list = true){
        /**Obtendo o container contendo as operações básicas envolvendo o doctrine. */
        $doctrine_container = \Zend_Registry::get('doctrine');
        $em = $doctrine_container->getEntityManager();
        
        $query = $em->createQuery('select s from ' . self::$doctrine_class_namespace . '\SignatureDoctrine s');
        
        if($paginated_list){
            // Paginação ativada
            $mapper = SignatureMapper::getInstance();
            
            return new \DoctrineExtensions\Paginate\PaginationAdapter($query, $mapper, null);
        }
                
        $object_list = $query->getResult();
        
        $object_business_list = array();
        foreach($object_list as $object){
            $object_business_list[] = SignatureMapper::getInstance()->doctrineEntityToBusiness($object);
        }
        
        return $object_business_list;        
    }
    
} // class

