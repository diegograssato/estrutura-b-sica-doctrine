<?php

namespace GuestBook\Model\BusinessLayer;

use GuestBook\Model\Doctrine\DAO\SignatureDAO;

use GuestBook\Model\BusinessLayer\GuestBook;
use Authenticator\Model\BusinessLayer\User;

/**
 * Representa uma assinatura a um determinado guestbook, i. e., 
 * contém o usuário (e a data) que assinou e o livro de visita assinado.
 *
 * @author maatao
 */
class Signature {
    
    private $id;
    public function setId($id){$this->id = $id;}
    public function getId(){return $this->id;}
    
    private $creation_date;
    public function setCreationDate($creation_date){$this->creation_date=$creation_date;}
    public function getCreationDate(){return $this->creation_date;}
    
    /** 
     * 
     * A mensagem personalizada do usuário durante a assinatura do livro
     * 
     */
    private $message;
    public function setMessage($message){$this->message=$message;}
    public function getMessage(){return $this->message;}
    
    private $guestbook;
    public function setGuestBook($guestbook){
        if(!$guestbook instanceof GuestBook){
            throw new \Zend_Exception('Objeto não é uma instância de GuestBook');
        }
        $this->guestbook = $guestbook;
    }
    public function getGuestBook(){return $this->guestbook;}
    
    private $user;
    public function setUser($user){
        if(!$user instanceof User){
            throw new \Zend_Exception('Objeto não é uma instância de User. ');
        }
        $this->user = $user;
    }
    public function getUser(){return $this->user;}
    
    /**Adiciona a assinatura no Banco de Dados*/
    public function saveSignature(){
        return SignatureDAO::add($this);
    }
    
    /**
     * Obtem um determinado objeto (identificado pelo id passado)
     * da respectiva classe do Banco de Dados
     */
    public static function getSignature($id){
        return SignatureDAO::get($id);
    }
    
    /**
     * Retorna a lista de objetos existentes no Banco de Dados. 
     */
    public static function listSignatures(){
        return SignatureDAO::listAll();
    }
    
}

