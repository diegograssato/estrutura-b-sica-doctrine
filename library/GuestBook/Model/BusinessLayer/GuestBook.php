<?php

namespace GuestBook\Model\BusinessLayer;

use GuestBook\Model\Doctrine\DAO\GuestBookDAO;
use GuestBook\Model\Doctrine\DAO\SignatureDAO;

/**
 * Representa um livro de visitas.
 *
 * @author maatao
 */
class GuestBook {
    
    private $id;
    public function setId($id){$this->id = $id;}
    public function getId(){return $this->id;}
    
    /**O titulo (ou mensagem) personalizada do usuário. */
    private $title;
    public function setTitle($title){$this->title=$title;}
    public function getTitle(){return $this->title;}
    
    private $creation_date;
    public function setCreationDate($creation_date){$this->creation_date=$creation_date;}
    public function getCreateionDate(){return $this->creation_date;}
    
    /**Adiciona o guestbook no Banco de Dados*/
    public function saveGuestBook(){
        return GuestBookDAO::add($this);
    }
    
    /**Obtem um determinado objeto (identificado pelo id passado) guestbook do Banco de Dados*/
    public static function getGuestBook($id){
        return GuestBookDAO::get($id);
    }
    
    /**Retorna a lista de GuestBooks existente no BD*/
    public static function listGuestBooks(){
        return GuestBookDAO::listAll();
    }
    
    /**Retorna a lista de assinaturas para o livro de visitas. */
    public function listSignatures(){
        return SignatureDAO::listAll();
    }
    
    
}

